CREATE TABLE IF NOT EXISTS `cita_simpsons` (
  `id` int NOT NULL AUTO_INCREMENT,
  `frase` text NOT NULL,
  `personaje` varchar(100) NOT NULL,
  `capitulo` int(11) NOT NULL,
  `temporada` int(11) NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cita_simpsons` (`id`, `frase`, `personaje`, `capitulo`, `temporada`) VALUES (NULL, 'Un servidor da la bienvenida a nuestros nuevos amos.', 'Kent Brockman', '15', '5');

INSERT INTO `cita_simpsons` (`id`, `frase`, `personaje`, `capitulo`, `temporada`) VALUES (NULL, 'Hijos, lo intentásteis al máximo y fracasásteis. La lección es: no os esforcéis.', 'Homer Simpson', '18', '5');

INSERT INTO `cita_simpsons` (`id`, `frase`, `personaje`, `capitulo`, `temporada`) VALUES (NULL, 'A mí no me mires, nena. Yo voté a Kodos.', 'Homer Simpson', '1', '8');

INSERT INTO `cita_simpsons` (`id`, `frase`, `personaje`, `capitulo`, `temporada`) VALUES (NULL, 'El aliento de mi gato huele a comida de gato.', 'Ralph Wiggum', '2', '6');
