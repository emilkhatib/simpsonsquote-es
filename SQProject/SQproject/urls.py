# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib import admin
from SQapp.views import citaAleatoria, buscarCita, ClaseBusqueda
from rest_framework.authtoken.views import obtain_auth_token


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^citaaleatoria/', citaAleatoria),
    url(r'^buscar/', ClaseBusqueda.as_view()),
    url(r'^buscar-html/', buscarCita),
    url(r'^api-token-auth/', obtain_auth_token),
]
