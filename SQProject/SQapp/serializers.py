# -*- coding: utf-8 -*-
from rest_framework import serializers
from SQapp.models import CitaSimpsons

# Este serializador es el más sencillo posible. Explicado en http://www.emilkhatib.es/introduciento-django-rest-framework/
class SerializadorCS(serializers.ModelSerializer):
	class Meta:
		model = CitaSimpsons

