# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from SQapp.models import CitaSimpsons
from SQapp.serializers import SerializadorCS


# Primer ejemplo usado en la introducción a Django (http://www.emilkhatib.es/sirviendo-citas-de-los-simpson-con-django/)
def citaAleatoria(request):
	cita = CitaSimpsons.objects.order_by('?').first() # Ordenar aleatoriamente y tomar la primera entrada
	respuesta = '\"' + cita.frase + '\" ' + cita.personaje + ' (T' + str(cita.temporada) + 'C' + str(cita.capitulo) + ')' 
	return HttpResponse(respuesta) # Devolver un texto plano


# Segundo ejemplo usando plantillas. (http://www.emilkhatib.es/usando-plantillas-en-django/)
# Para usar esta vista, hay que desactivar la autenticación. Para ello cambiar 'DEFAULT_PERMISSION_CLASSES' a 
# 'rest_framework.permissions.AllowAny' en settings.py

def buscarCita(request):
	if request.method == 'GET':
		# Para la primera petición del usuario, devolver un formulario de búsqueda
		return render(request, 'buscar.html')
	else:
		# Para la segunda petición, leer los parámetros posteados y devolver los resultados de búsqueda
		palabras = request.POST['palabras'].split()  # Separar los términos de búsqueda en una lista
		if len(palabras) >= 1:
			resultados = CitaSimpsons.objects.filter(frase__icontains = palabras[0]) # Filtrar las citas por la frase que contiene el primer término
			for palabra_adicional in palabras[1:]: # Filtrar por palabras adicionales
				resultados = resultados.filter(frase__icontains = palabra_adicional)
			if len(resultados) > 0:
				return render(request, 'resultados.html', {'resultados':resultados}) # Renderizar y devolver los resultados
			else:
				return render(request, 'error.html', {'mensaje':'No hay resultados'}) # Avisar al usuario si no hay resultados
		else:
			# Avisar al usuario de que no ha insertado términos de búsqueda
			return render(request, 'error.html', {'mensaje':'Por favor, introduce términos de búsqueda'})


# Este ejemplo implementa una vista basada en clase de REST Framework (http://www.emilkhatib.es/introduciento-django-rest-framework/)
class ClaseBusqueda(APIView):
	def get(self, request):
		# Para la primera petición del usuario, devolver un formulario de búsqueda. Con la versión final del protocolo (http://www.emilkhatib.es/autenticacion-con-django-rest-framework/) este método cae en desuso
		return render(request, 'buscar.html')

	def post(self, request):
		# Segunda petición, funciona como en el caso anterior; sustituyendo las plantillas por serializadores
		palabras = request.POST['palabras'].split()  # Separar los términos de búsqueda en una lista
		if len(palabras) >= 1:
			resultados = CitaSimpsons.objects.filter(frase__icontains = palabras[0]) 
			for palabra_adicional in palabras[1:]: 
				resultados = resultados.filter(frase__icontains = palabra_adicional)
			if len(resultados) > 0:
				serializador = SerializadorCS(resultados, many=True)
				return Response(serializador.data)
			else:
				return Response({'mensaje':'No hay resultados'})
		else:
			return Response({'mensaje':'Por favor, introduce términos de búsqueda'})
	
